#ifndef __MyDictionary_h
#define __MyDictionary_h

#include <map>
#include "Dictionary.h"
#include "NotFoundException.h"

template<class TKey, class TValue> class MyDictionary: public Dictionary<TKey, TValue>
{
public:
	const TValue& Get(const TKey& key) const;
	void Set(const TKey& key, const TValue& value);
	bool isSet(const TKey& key);
private:
	std::map<TKey, TValue> dictionary;
};

template<class TKey, class TValue>
const TValue& MyDictionary<TKey, TValue>::Get(const TKey& key) const
{
	if (dictionary.count(key) == 1) {
		return dictionary.find(key)->second;
	} else {
		throw NotFoundExceptionImpl<TKey>(key);
	}
}

template<class TKey, class TValue>
void MyDictionary<TKey, TValue>::Set(const TKey& key, const TValue& value)
{
	if (dictionary.count(key) == 1) {
		dictionary[key] = value;
	} else {
		dictionary.insert(std::pair<char,int>(key,value));
	}
}

template<class TKey, class TValue>
bool MyDictionary<TKey, TValue>::isSet(const TKey& key)
{
	if (dictionary.count(key) == 1) {
		return true;
	} else {
		return false;
	}
}

#endif