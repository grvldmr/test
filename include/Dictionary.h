#ifndef __Dictionary_h
#define __Dictionary_h

template<class TKey, class TValue>
class Dictionary
{
public:
	virtual ~Dictionary() = default;

	virtual const TValue& Get(const TKey& key) const = 0;
	virtual void Set(const TKey& key, const TValue& value) = 0;
	virtual bool isSet(const TKey& key) = 0;
};

#endif 

