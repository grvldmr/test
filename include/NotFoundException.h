#ifndef __NotFoundException_h
#define __NotFoundException_h

template<class TKey>
class NotFoundException: public std::exception
{
public:
	virtual const TKey& GetKey() const noexcept = 0;
};

template<class TKey>
class NotFoundExceptionImpl: NotFoundException<TKey>
{
public:
	NotFoundExceptionImpl(const TKey& key): _key(key) {}
	const TKey& GetKey() const noexcept;
private:
	TKey _key;
};

template<class TKey>
const TKey& NotFoundExceptionImpl<TKey>::GetKey() const noexcept
{
	return _key;
}

#endif 