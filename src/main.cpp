#include <iostream>
#include "MyDictionary.h"

int main ()
{
	MyDictionary<int, int> md;
	md.Set(1,777);
	md.Set(1,66);
	md.Set(2,3333);
	std::cout << "Element with a key 1 has value " << md.Get(1) << std::endl;
	std::cout << "Element with a key 2 has value " << md.Get(2) << std::endl;
	std::cout << "Was the element with a key 1 assigned: " << std::boolalpha << md.isSet(1) << std::endl;
	std::cout << "Was the element with a key 3 assigned: " << std::boolalpha << md.isSet(3) << std::endl;
	try {
		std::cout << "The element with a key 3" << std::endl;
		std::cout << md.Get(3);
		std::cout << std::endl;
	} catch(NotFoundExceptionImpl<int>& e) {
		std::cout << "Trying to access for a non existing key ";
		std::cout << e.GetKey();
		std::cout << std::endl;
	}
	return 0;	
}